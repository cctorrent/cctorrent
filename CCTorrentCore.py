#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   This file is part of cctorrent
#   Copyright (C) 2014-2015 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#This file contains the core functionality of CCTorrent
#to form an abstraction layer between frontends and CCTorrent functionality.

import os
from datetime import datetime
import subprocess
from django.db.models import Max
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User

from cctorrent.models import Album, Artist, Track, License, User, Tag, Rating, DonationInfo, UploadedAudioFile
from cctorrent.apps import CCTorrentConfig
from cctorrent.helpers import newUploadedTrack, convertUploadedImage
from cctorrent.ConverterThread import ConverterThread
from cctorrent.extensions.Torrent import Torrent

class CCTorrentException(Exception):
  def __init__(self, message, code):
    pass #to be done



def CheckFFmpegInstallation():
  returnCode = subprocess.call(['ffmpeg', '-v', '0']) #this does nothing
  if(returnCode == 1):
    #ffmpeg is there and exited with code 1 because we used it wrong. So this is a proof it's there!
    return True
  #in all other cases something went wrong: return False
  return False


def CheckAudioFileUploadDirectory():
  if (not os.path.exists(CCTorrentConfig.FileDirectory+'convert/')):
    try:
      os.mkdir(CCTorrentConfig.FileDirectory+'convert/')
      return True
    except OSError:
      print("ERROR: Cannot create audio file upload directory at: "+ CCTorrentConfig.FileDirectory+'convert/')  
      return False
  else:
    return True


def SplitTagString(tagString=None):
  if(not isinstance(tagString, basestring)):
    raise Exception("SplitTagString requires a string as input!")
  return tagString.replace(' ','').lower().split('#')[1:]


def ResolveLicenseShort(licenseShort=None):
  if(not isinstance(licenseShort, basestring)):
    raise Exception("ResolveLicenseShort requires a string as input!")
  license = None
  try:
    license = License.objects.get(short_name=licenseShort)
  except ObjectDoesNotExist:
    raise Exception("License short name unknown! Cannot resolve short name to a valid license object!")
  return license


def VerifyTags(tags=None):
  if(tags==None):
    raise Exception("VerifyTags requires a list of tags!")
  if(not isinstance(tags, list)):
    raise Exception("VerifyTags requires a list object!")
  if(len(tags) < 1):
    raise Exception("VerifyTags requires a list with at least one tag in it!")

  tagList = []
  print "Tags="+unicode(tags)
  for t in tags:
    if(len(t) < CCTorrentConfig.MinimumTagLength):
      raise Exception("Tag \""+unicode(t)+"\" is too short. Minimum length is "+str(CCTorrentConfig.MinimumTagLength)+"!")
    #a valid tag was found if this is executed
    tagObject = None
    try:
      tagObject = Tag.objects.get(pk=t)
      tagList.append(tagObject)
    except ObjectDoesNotExist:
      #tag does not exist: create it
      tagObject = Tag()
      tagObject.name = t
      tagObject.save()
      tagList.append(tagObject)
  return tagList


def CreateArtist(user=None, artistName=None, description=None, tags=None, artistRawPictureFile=None):
  #TODO: do all stuff related to creating an artist, not just the directory creation!
  if(not isinstance(user, User)):
    raise Exception("CreateArtists needs a valid django user object!")
  if((len(artistName) == 0) or (artistName == None)):
    #we don't accept empty names or None strings:
    raise Exception("Artist name is empty: Please specify an artist name!")
  if(len(artistName) < CCTorrentConfig.MinimumArtistNameLength):
    raise Exception("Artist name is too short. Please use a logner name!")
  
  if(len(Artist.objects.filter(name=artistName)) > 0):
    raise Exception("Artist name already in use: Please use another name for this artist!")
    
  artist = Artist()
  artist.name = artistName
  artist.user = user
  if(description != None):
    artist.description = str(description)
  
  #check for tags (if any) and add them to the artist:
  tagList = None
  if(tags != None):
    tagList = VerifyTags(tags)
    
  #if the following is executed every check went fine 
  #and the new artist is stored in the database:
  artist.save()
    
  #check for tags (if any) and add them to the artist:
  if(tags != None):
    artist.tags = tagList
    artist.save()
  
  #make artist directory:
  try:
    #print "New dir = "+CCTorrentConfig.FileDirectory+'artist/'+str(artist.id) #DEBUG
    os.mkdir(CCTorrentConfig.FileDirectory+'artist/'+str(artist.id))
  except OSError:
    #TODO: check if directory exists or if the directory can't be created (which would be a severe problem)
    print("WARNING: CreateArtist: Artist directory already exists!")  
  
  if(artistRawPictureFile != None):
    artistImage = convertUploadedImage(artistRawPictureFile)
    if(artistImage != None):
      #image conversion successful:
      artistImage.save(CCTorrentConfig.FileDirectory+'artist/'+str(artist.id)+'/picture.jpg', 'JPEG', quality=CCTorrentConfig.ImageQuality)
    else:
      raise Exception("Cannot convert artist image!")
  
  return artist


def CreateAlbum(name, publicationDate, licenseShort, albumCover, artistId, tags=None):
  #do some checks first:
  
  #data types:
  if(not isinstance(name, basestring)):
    raise Exception("Error: Album name must be a string!")
  else:
    if(len(name) == 0):
      raise Exception("Error: Album name must not be empty!")
    elif(len(name) < CCTorrentConfig.MinimumAlbumNameLength):
      raise Exception("Error: Album name is too short!")
  
  if(not isinstance(publicationDate, datetime)):
    raise Exception("Error: Album publication date must be a date instance!")
  
  if(not isinstance(licenseShort, basestring)):
    raise Exception("Error: License short name must be a string!")
  
  if(not isinstance(artistId, int)):
    raise Exception("Error: Artist ID must be an integer!")
  if(artistId < 1):
    raise Exception("Error: Artist ID must be an integer greater zero!")
  
  artist = Artist.objects.get(pk=artistId) 
  #(if the artist ID is not in the database Django will raise an Exception)
  
  #check for double names:
  if(len(artist.album_set.filter(name=name)) > 0):
    raise Exception("Album name already in use: Please use another name for this album!")
  
  
  #process data:
  newAlbum = Album()
  newAlbum.name = name
  newAlbum.pub_date = publicationDate
  newAlbum.license = ResolveLicenseShort(licenseShort)
  newAlbum.artist = artist
  
  #check for tags (if any) and add them to the album:
  tagList = None
  if(tags != None):
    tagList = VerifyTags(tags)
    
  newAlbum.save()
  #album is in database now
  
  if(tagList != None):
    newAlbum.tags = tagList
    newAlbum.save()
    
  #create album directory:
  try:
    os.mkdir(CCTorrentConfig.FileDirectory+'album/'+newAlbum.GetAlbumDirName())
  except OSError:
    #TODO: error handling
    #print ("Directory already exists!")
    pass
  
  #convert and save cover file in album directory
  #TODO: read https://docs.djangoproject.com/en/1.7/topics/security/#user-uploaded-content-security
  coverFile = None
  try:
    coverFile = convertUploadedImage(albumCover)
  except IOError:
    raise Exception("Cover file format error!") #TODO: improve error handling: use standard image
  
  if(coverFile == None):
    raise Exception("Cover file coudln't be converted/saved!")
  #if the following is executed everything went fine: save cover image
  coverFile.save(CCTorrentConfig.FileDirectory+'album/'+newAlbum.GetAlbumDirName()+'/cover.jpg', 'JPEG', quality=CCTorrentConfig.ImageQuality)
  
  #and return album id:
  return newAlbum.id #TODO: return new album instead of just the ID


def CreateLicenseFile(album):
  if(not isinstance(album, Album)):
    raise Exception("Error: CreateLicenseFile needs an Album instance!")
  #just make a csv file since it's easy to parse:
  csv = "entity;license;\n"
  csv += "cover.jpg;"+str(album.license.url)+"\n"
  for t in album.track_set.all():
    csv += 'track '+'{:02d}'.format(t.number)+';'+str(t.license.url)+"\n"
  f = open(CCTorrentConfig.FileDirectory+'album/'+album.GetAlbumDirName()+'/license.csv','w')
  f.write(csv)
  f.close()
  
  
def FinishAlbum(album):
  if(not isinstance(album, Album)):
    raise Exception("Error: FinishAlbum needs an Album instance!")
  #we have to create the torrent fil|e here:
  
  albumPrefix = album.GetAlbumDirName()
  torrent = None
  if(CCTorrentConfig.DemoMode == False):
    torrent = Torrent(name=albumPrefix, creator="CCTorrent",
                      private=False,
                      comment="This torrent was created using CCTorrent, a web application for Creative Commons licensed music sharing. Enjoy the music you\'re downloading :)")
    #tracker list from 2015-10-21
    #TODO: make this configurable!
    #the tracker list is only 
    torrent.AddTracker('udp://tracker.openbittorrent.com:80')
    torrent.AddTracker('udp://open.demonii.com:1337')
    torrent.AddTracker('udp://tracker.coppersurfer.tk:6969')
    torrent.AddTracker('udp://tracker.leechers-paradise.org:6969')
  else:
    #demo mode: no trackers here, and the torrent is private
    torrent = Torrent(name=albumPrefix, creator="CCTorrent",
                      private=True,
                      comment="This torrent was created using CCTorrent, a web application for Creative Commons licensed music sharing. Enjoy the music you\'re downloading :)")
  #the web seed is always added (otherwise the demo wouldn't make any sense)
  torrent.AddWebSeed(str(CCTorrentConfig.FileURL+'album/'))
  
  
  
  for track in album.track_set.all():
    torrent.AddFile(CCTorrentConfig.FileDirectory+'album/'+albumPrefix+'/'+track.GetFileName()+'.ogg')
  torrent.AddFile(CCTorrentConfig.FileDirectory+'album/'+albumPrefix+'/cover.jpg')
  CreateLicenseFile(album)
  torrent.AddFile(CCTorrentConfig.FileDirectory+'album/'+albumPrefix+'/license.csv')
  try:
    torrentFile = open(CCTorrentConfig.FileDirectory+'album/'+albumPrefix+'.torrent', 'wb')
    torrentFile.write(torrent.Bencode())
    torrentFile.close()
  except Exception as e:
    print("Error writing torrent file: "+str(e))
    return -1
  
  #if code execution has reached this point everything went fine
  album.finished = True
  album.save()
  return 0


def CreateTrack(name, number, albumId, artistId, licenseShort, trackData=None, tags=None):
  if(not isinstance(name, basestring)):
    raise Exception("Error: Track name must be a string!")
  else:
    if(len(name) == 0):
      raise Exception("Error: Track name must not be empty!")
    elif(len(name) < CCTorrentConfig.MinimumTrackNameLength):
      raise Exception("Error: Track name is too short!")
  
  if(not isinstance(number, int)):
    raise Exception("Error: Track number must be an integer!")
  else:
    if(number < 1):
      raise Exception("Error: Track number must be an integer greater zero!")
  
  if(not isinstance(albumId, int)):
    raise Exception("Error: Album ID must be an integer!")
  else:
    if(albumId < 1):
      raise Exception("Error: Album ID must be an integer greater zero!")
  
  if(not isinstance(artistId, int)):
    raise Exception("Error: Artist ID must be an integer!")
  else:
    if(artistId < 1):
      raise Exception("Error: Artist ID must be an integer greater zero!")
  
  if(not isinstance(licenseShort, basestring)):
    raise Exception("Error: License short name must be a string!")
  
  if(trackData == None):
    raise Exception("Error: Track data must be specified!")
  
  track = Track()
  track.name = name
  track.album = Album.objects.get(pk=albumId)
  track.artist = track.album.artist #TODO: allow track artist to be different from album artist (for collections)
  track.number = number
  
  
  #do checks:
  
  #check for double names:
  if(len(track.album.track_set.filter(name=track.name)) > 0):
    raise Exception("Track name already in use: Please use another name for this track!")
  
  #check for double track numbers:
  if(len(track.album.track_set.filter(number=track.number)) > 0):
    raise Exception("Track number already in use: Please use another number for this track! (Next free track number is "+str(track.album.track_set.aggregate(Max('number'))['number__max']+1)+")")
  
  #resolve the license short name to a license:
  track.license = ResolveLicenseShort(licenseShort)
  #try:
    #track.license = License.objects.get(short_name=licenseShort)
  #except ObjectDoesNotExist:
    #raise Exception("Invalid license: Please choose a valid license!")
  
  #check for tags (if any) and add them to the track:
  tagList = None
  if(tags != None):
    tagList = VerifyTags(tags)
  
  #if the following is executed every check was successful and the track is saved
  track.save()
  
  if(tagList != None):
    track.tags = tagList
  
  #everything went fine until here so that we can save the track:
  track.save()
  
  newUploadedTrack(trackData = trackData, track=track) #this adds the file name into the database and starts the conversion as background job


  #TODO: run as background job!!!!
  #TODO: check for wav, ogg vorbis or flac! everything else is unsupported!
  #TODO: strip leading ".", slashes and other characters that may navigate to other parts of the file system => DONE (using md5sum of file as file name)
  #TODO: read https://docs.djangoproject.com/en/1.7/topics/security/#user-uploaded-content-security
  
  #the album directory should already exist here. If not that's an error!
  #TODO: write integrity check function to sync the data directory from the database
  return track


def ChangeTrackLicense(track, newLicense):
  track = Track.objects.get(pk=trackId)
  if(track.album.finished == True):
    raise Exception("Error: Cannot change track license since the album \""+unicode(album.name)+"\" was finished!")
  if(not isinstance(newLicense, License)):
    raise Exception("Error: New track license must be a License instance!")
  
  track.license = newLicense
  track.save()

def ChangeTrackDetails(track, name=None, number=None, license=None):
  if(not isinstance(track, Track)):
    raise Exception("ChangeTrackDetails needs a track instance!")
  if(track.album.finished == True):
    raise Exception("Error: Cannot change track details since the album \""+unicode(album.name)+"\" was finished!")
  
  if((name != None) and (isinstance(name, basestring))):
    #change name:
    track.name = name
  if((number != None) and (isinstance(number, int))):
    if(number <= 0):
      raise Exception("Track number must be greater than zero!")
    #change number:
    track.number = number
  if((license != None) and (isinstance(license, License))):
    #change license:
    track.license = license
  
  #end of function: save changes:
  track.save()