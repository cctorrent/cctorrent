#
#   This file is part of cctorrent
#   Copyright (C) 2014-2015 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
from datetime import datetime
from PIL import Image as PILImage
from django.utils import timezone
from django.db.models import Max
#from django.core.files import File

from cctorrent.CCTorrentCore import *

from django.shortcuts import render, render_to_response, redirect
from cctorrent.models import Album, Artist, Track, License, User, Tag, Rating, DonationInfo
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.conf import settings as django_settings
from django.core.exceptions import ObjectDoesNotExist
#from cctorrent.settings import settings.FileDirectory, CCTORRENT_FILEURL
#from cctorrent.settings import settings
from cctorrent.apps import CCTorrentConfig #as settings
from cctorrent.helpers import convertUploadedImage
from cctorrent.forms.artist import NewArtistForm
from cctorrent.forms.album import NewAlbumForm
from cctorrent.forms.track import NewTrackForm
from cctorrent.forms.search import GeneralSearchForm
from cctorrent.forms.importJamendo import JamendoImportForm
from cctorrent.extensions.JamendoImport import JamendoImportThread

#from cctorrent.apps import CCTorrentConfig #planned for Django 1.7


def index(request):
  #return HttpResponse("Index site!")
  #latest_albums = Album.objects.all().order_by('-pub_date')[:5] #old
  latest_albums = Album.objects.filter(pub_date__lt=datetime.now()).order_by('-pub_date')[:5] #new: only those albums which are already published
  newest_artists = Artist.objects.all().order_by('-id')[:5]
  
  return render_to_response('cctorrent/index.html', 
    {'loggedinUser':request.user,
    'authenticated':request.user.is_authenticated(),
    'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
    'latest_albums': latest_albums,
    'newest_artists': newest_artists,
    'CCTorrentConfig':CCTorrentConfig,
    }
    )


def api(request):
  return HttpResponse('{"Status":"Error", "Reason":"Not implemented yet!"}')
  
  
def settingsJS(request):
  return render_to_response('cctorrent/settings.js',
      {
        'settings':CCTorrentConfig,
      }
    )

#def loginView(request):
#  username = request.POST['username']
#  password = request.POST['password']
#  user = authenticate(username=username, password=password)
#  if user is not None:
#    if user.is_active:
#      login(request, user)
#      return render_to_response('welcome.html', {'user': user})
#    else:
#      return render_to_response('cctorrent/login.html', {'success':false, 'disabled':true})
#  else:
#    return render_to_response('cctorrent/login.html', {'success':false, 'disabled':false})

def logoutView(request):
  if request.user.is_authenticated():
    logout(request)
    return render_to_response('cctorrent/logout.html', {
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTorrentConfig':CCTorrentConfig,
      })


def newartist(request, user_id):
  u = User.objects.get(pk=user_id)
  errorList = []
  if (request.method == 'POST'):
    #the user filled and sent the form to create a new artist: Handle the data
    newArtistForm = NewArtistForm(request.POST, request.FILES)
    newArtist = None
    if newArtistForm.is_valid():
      try:
        newArtist = CreateArtist(user=u,
                      artistName=newArtistForm.cleaned_data['name'],
                      description=newArtistForm.cleaned_data['description'],
                      tags=SplitTagString(newArtistForm.cleaned_data['tags']),
                      artistRawPictureFile=request.FILES['picture'])
      except Exception as e:
        #something went wrong when creating the new artist: either some checks failed or database or file system errors occured
        errorList.append("Exception occured when creating a new artist: "+str(e))
      #the form is valid: continue processing data:
      
    else:
      errorList.append("Invalid form! Please make sure that all required fields are filled with valid data!")
    
    
    #check if errors orruced:
    if(len(errorList) == 0):
      #there were no errors: redirect to the new artist's page
      return redirect('artist', newArtist.id)
    else:
      #there were errors: return to the new artist form
      return render(request, 'cctorrent/newartist.html', {
        'form': newArtistForm,
        'user':u,
        'loggedinUser':request.user,
        'authenticated':request.user.is_authenticated(),
        'errorList':errorList,
        'CCTorrentConfig':CCTorrentConfig,
        })
  else:
    #the user clicked on "new artist": Provide an empty form
    newArtistForm = NewArtistForm()
    return render(request, 'cctorrent/newartist.html', {
      'form': newArtistForm,
      'user':u,
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTorrentConfig':CCTorrentConfig,
      })


def artist(request, artist_id):
  #  return HttpResponse("Artist %s selected" % artist_id)
  try:
    a = Artist.objects.get(pk=artist_id)
  except Artist.DoesNotExist:
    raise Http404
  
  #edit = (request.user.is_authenticated() and (request.user.username == artist.user.username)) #no if-else statement needed: these are in the artist.html template
  edit = False #only for development!
  return render_to_response('cctorrent/artist.html',
    {
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
      'artist': a,
      'albums': a.album_set.filter(pub_date__lt=timezone.now()).order_by('-pub_date'), #new: only those albums which are already published
      'edit': edit,
      'CCTorrentConfig':CCTorrentConfig,
    })


def newtrack(request, album_id=None):
  album = Album.objects.get(pk=album_id)
  errorList = []
  newTrackForm = None
  if(request.user.is_authenticated()):
    if(request.user == album.artist.user):
      if (request.method == 'POST'):
        #handle the request already sent
        newTrackForm = NewTrackForm(request.POST, request.FILES)
        if newTrackForm.is_valid():
          #process data:
          try:
            CreateTrack(
              name=newTrackForm.cleaned_data['name'],
              number=newTrackForm.cleaned_data['number'],
              albumId = int(album_id),
              artistId = album.artist.id,
              licenseShort = unicode(newTrackForm.cleaned_data['licenseShort']),
              tags = SplitTagString(newTrackForm.cleaned_data['tags']),
              trackData = request.FILES['musicFile'].read())
          except Exception as e:
            errorList.append("Exception when creating a new track: "+str(e))
        else:
          #form is invalid
          errorList.append("Invalid form!")
    
      else:
        #the user opens the form without sending data to it
        errorList = []
        
        #we have to check if the audio file upload directory is there or can be created:
        if(CheckAudioFileUploadDirectory == False):
          #directory can't be created
          errorList.append('Cannot create audio file upload directory! Audio file upload will fail!')
        
        #we have to check if ffmpeg is working!
        if(CheckFFmpegInstallation() == False):
          #ffmpeg is not working
          errorList.append('Your FFmpeg installation is not working! Audio file conversion will fail!')
        
        #show the form
        newTrackForm = NewTrackForm()
        return render(request, 'cctorrent/newtrack.html', {
          'form': newTrackForm,
          'loggedinUser':request.user,
          'authenticated':request.user.is_authenticated(),
          'album':album,
          'errorList':errorList, #empty lists are False
          'CCTorrentConfig':CCTorrentConfig,
          })
    
    else:
      #the user is not the user that owns the album
      errorList.append("Not allowed: You are not the user that owns the album and thus cannot add a new track to it!")
  else:
    #the user is not logged in
    errorList.append("Not allowed: You are not logged in and thus cannot add a new track to the album. Please log in and try again!")
  
  if(len(errorList) == 0):
    #no errors
    return redirect('album', album_id)
  else:
    #at least one error appeared:
    return render(request, 'cctorrent/newtrack.html', {
      'form': newTrackForm,
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'album':album,
      'errorList':errorList,
      'CCTorrentConfig':CCTorrentConfig,
      })


def newalbum(request, artist_id=None):
  artist = Artist.objects.get(pk=artist_id)
  if (request.method == 'POST'):
    errorList = []
    albumId = 0
    #handle the request already sent
    newAlbumForm = NewAlbumForm(request.POST, request.FILES)
    if (newAlbumForm.is_valid()):
      #process data:
      try:
	albumId = CreateAlbum(
	  name=newAlbumForm.cleaned_data['name'],
	  publicationDate=newAlbumForm.cleaned_data['publicationDate'],
	  licenseShort = unicode(newAlbumForm.cleaned_data['licenseShort']),
	  albumCover=request.FILES['cover'],
	  tags = SplitTagString(newAlbumForm.cleaned_data['tags']),
	  artistId =int(artist_id)
	  )
	print("AlbumID= "+str(albumId))
      except Exception as e:
        errorList.append("Exception when creating new album: "+str(e))
    else:
      errorList.append("Invalid form!")
    
    if(len(errorList) == 0):
      #no errors:
      return redirect('album', albumId)
    else:
      return render(request, 'cctorrent/newalbum.html', {
      'form': newAlbumForm,
      'artist':artist,
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'errorList':errorList,
      'CCTorrentConfig':CCTorrentConfig,
      })
  else:
    newAlbumForm = NewAlbumForm()
    return render(request, 'cctorrent/newalbum.html', {
      'form': newAlbumForm,
      'artist':artist,
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTorrentConfig':CCTorrentConfig,
      })



def album(request, album_id):
    #return HttpResponse("Album %s selected" % album_id)
  try:
    a = Album.objects.get(pk=album_id)
  except Album.DoesNotExist:
    raise Http404
  
  if((a.pub_date > timezone.now()) and (request.user.id != a.artist.user.id)):
    #the album was not published yet and the user is not the creator of the album!
    return HttpResponse("Error: Album not yet published!") #TODO: beautify!!!!
  else:
    edit = (request.user.is_authenticated() and (request.user.username == a.artist.user.username)) #no if-else needed here, if-else statements are in HTML template
    return render_to_response('cctorrent/album.html', {
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
      'album': a,
      'edit': edit,
      'CCTorrentConfig':CCTorrentConfig,
      })

  
def finishAlbum(request, albumId):
  errorList = []
  try:
    album = Album.objects.get(pk=albumId)
  except Album.DoesNotExist:
    raise Http404 #TODO: redirect to some nice place (e.g. artist page, if exists or the dashboard if nothing else exists)
  
  if(request.user.is_authenticated()):    
    if(request.user == album.artist.user):
      #only the owner may finish an album!
      try:
        FinishAlbum(album)
        return render(request, 'cctorrent/album.html', {
          'loggedinUser':request.user,
          'authenticated':request.user.is_authenticated(),
          'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
          'album': album,
          'errorList':None,
          'CCTorrentConfig':CCTorrentConfig,
          })
      except Exception as e:
        errorList.append(str(e))
    else:
      errorList.append("You are not the owner of this album and thus cannot finish the album!")
  else:
    errorList.append("You are not logged in and thus cannot finish an album. Please log in and try again!")
  
  if(len(errorList) > 0):
    #there were errors:
    return render(request, 'cctorrent/album.html', {
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
      'album': album,
      'errorList':errorList,
      'CCTorrentConfig':CCTorrentConfig,
      })


def tagsearch(request, tag):
  tagArtists = None
  tagAlbums = None
  tagTracks = None
  
  tagObject = Tag.objects.get(pk=tag)
  
  try:
    tagArtists = Artist.objects.filter(tags__name=tagObject.name)
  except ObjectDoesNotExist:
    tagArtists = None
  try:
    tagAlbums = Album.objects.filter(tags__name=tagObject.name)
  except ObjectDoesNotExist:
    tagAlbums = None
  try:
    tagTracks = Track.objects.filter(tags__name=tagObject.name)
  except ObjectDoesNotExist:
    tagTracks = None
  #...render the search page:
  return render(request, 'cctorrent/search.html', {
    'tagSearch':True,
    'tag':tag,
    'form': None,
    'artistResults':tagArtists,
    'albumResults':tagAlbums,
    'trackResults':tagTracks,
    'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
    'loggedinUser':request.user,
    'authenticated':request.user.is_authenticated(),
    'CCTorrentConfig':CCTorrentConfig,
    })




def searchresult(request, artistResults = None, albumResults = None, trackResults = None):
  searchForm = GeneralSearchForm()
  return render(request, 'cctorrent/search.html', {
    'tagSearch':False,
    'form': searchForm,
    'artistResults':artistResults,
    'albumResults':albumResults,
    'trackResults':trackResults,
    'loggedinUser':request.user,
    'authenticated':request.user.is_authenticated(),
    'CCTorrentConfig':CCTorrentConfig,
    })


    
def search(request, artistResults = None, albumResults = None, trackResults = None):
  if (request.method == 'POST'):
    #handle the request already sent
    searchForm = GeneralSearchForm(request.POST)
    if searchForm.is_valid():
      searchArtists = None
      searchAlbums = None
      searchTracks = None
      try:
        searchArtists = Artist.objects.filter(name__contains=searchForm.cleaned_data['term'])
      except ObjectDoesNotExist:
        searchArtists = None
      try:
        searchAlbums = Album.objects.filter(name__contains=searchForm.cleaned_data['term'])
      except ObjectDoesNotExist:
        searchAlbums = None
      try:
        searchTracks = Track.objects.filter(name__contains=searchForm.cleaned_data['term'])
      except ObjectDoesNotExist:
        searchTracks = None
      #...render the search page:
      return render(request, 'cctorrent/search.html', {
        'tagSearch':False,
        'form': searchForm,
        'artistResults':searchArtists,
        'albumResults':searchAlbums,
        'trackResults':searchTracks,
        'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
        'loggedinUser':request.user,
        'authenticated':request.user.is_authenticated(),
        'CCTorrentConfig':CCTorrentConfig,
        })
      #return redirect(search, artistResults=searchArtists, albumResults=searchAlbums, trackResults=searchTracks)
  else:
    searchForm = GeneralSearchForm()
    return render(request, 'cctorrent/search.html', {
      'tagSearch':False,
      'form': searchForm,
      'artistResults':artistResults,
      'albumResults':albumResults,
      'trackResults':trackResults,
      'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTorrentConfig':CCTorrentConfig,
      })
      
def torrentClients(request):
  return HttpResponse("Not implemented yet! Have a look at <a href=\"https://en.wikipedia.org/wiki/Comparison_of_BitTorrent_clients\">Wikipedia's comparison of BitTorrent clients</a>.")



def track(request, track_id):
    return HttpResponse("Track %s selected. (Site not implemented yet)" % track_id)
    
def dashboard(request):
  if request.user.is_authenticated():
    return render_to_response('cctorrent/dashboard.html', {
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
      'Notification':None,
      'CCTorrentConfig':CCTorrentConfig,
      })
  else:
    return HttpResponseRedirect(reverse('login', next='userpage'))

def usersettings(request):
  if request.user.is_authenticated():
    return render_to_response('cctorrent/usersettings.html', {
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'user': request.user,
      'CCTorrentConfig':CCTorrentConfig,
      })
  else:
    return HttpResponseRedirect(reverse('login', next='usersettings'))

    
def user(request, user_id):
  try:
    u = User.objects.get(pk=user_id)
    
  except User.DoesNotExist:
    raise Http404
  if request.user.is_authenticated() and (request.user.username == u.username):
    return render_to_response('cctorrent/user.html', {
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'user': u,
      'user_artists': u.artist_set.all(),
      'edit':True,
      'CCTorrentConfig':CCTorrentConfig,
      })
  else:
    return render_to_response('cctorrent/user.html', {
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'user': u,
      'user_artists': u.artist_set.all(),
      'edit':False,
      'CCTorrentConfig':CCTorrentConfig,
      })



def importJamendo(request):
  if (request.method == 'POST'):
    #the user sent a jamendo ZIP
    jiForm = JamendoImportForm(request.POST, request.FILES)
    if(jiForm.is_valid()):
      try:
        #jiThread = JamendoImportThread(user=request.user, albumArtist=jiForm.cleaned_data['albumArtist'],
        jiThread = JamendoImportThread(user=request.user, albumArtistName=jiForm.cleaned_data['albumArtistName'],
          albumName=jiForm.cleaned_data['albumName'],
          #zipURL = jiForm.cleaned_data['zipURL'])
          zipFile = jiForm.cleaned_data['zipFile'])
        jiThread.start()
        
        #render the dashboard page
        return render_to_response('cctorrent/dashboard.html', {
          'loggedinUser':request.user,
          'authenticated':request.user.is_authenticated(),
          'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
          'Notification':"The Jamendo album is being imported and will be visible soon!",
          'CCTorrentConfig':CCTorrentConfig,
          })
      except Exception as e:
        return render(request, 'cctorrent/importJamendo.html', {
          'form': jiForm,
          'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
          'loggedinUser':request.user,
          'authenticated':request.user.is_authenticated(),
          'errorList':[str(e)],
          'CCTorrentConfig':CCTorrentConfig,
          })
    else:
      return render(request, 'cctorrent/importJamendo.html', {
          'form': jiForm,
          'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
          'loggedinUser':request.user,
          'authenticated':request.user.is_authenticated(),
          'errorList':["Invalid form!"],
          'CCTorrentConfig':CCTorrentConfig,
          })
  else:
    jiForm = JamendoImportForm()
    return render(request, 'cctorrent/importJamendo.html', {
      'form': jiForm,
      'CCTORRENT_FILEURL': CCTorrentConfig.FileURL,
      'loggedinUser':request.user,
      'authenticated':request.user.is_authenticated(),
      'CCTorrentConfig':CCTorrentConfig,
      })

#TODO: serve the following as static files:
  
def logo(request):
  return render_to_response('cctorrent-logo.png')

  
def basicStyle(request):
  return render_to_response('BasicStyle.css')
  
def colorScheme(request):
  return render_to_response('ColorScheme.css')