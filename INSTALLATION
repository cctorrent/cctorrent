Please check first if your operating system meets the requirements,
specified in the file REQUIREMENTS.

If you have not created a Django project you first have to create one
in the desired directory:
django-admin startproject your_project_name

After that you can checkout the git branch of cctorrent
in the same directory:
git clone https://gitlab.com/cctorrent/cctorrent.git

Now you need to add cctorrent to the list of installed applications.
This list can be found in the file settings.py
in the directory that is named like your project.
(If your project is named foo1, then the file settings.py
is located in the subdirectory foo1).
Append the string 'cctorrent', to the application tuple
that is named INSTALLED_APPS to enable CCTorrent.

You have to make sure to set MEDIA_ROOT and MEDIA_URL
in your project's settings.py, otherwise storing
audio and images won't work and CCTorrent is unusuable.


In the file urls.py you have to include the CCTorrent URL file:
- First add the following line at the top of the urls.py file
of your django project:
from cctorrent.urls import cctorrent_urls
- After that you have to add an URL object to the urlpatterns tuple:
url(r'^cctorrent/', include(cctorrent_urls)),


Now you need to setup the database:
python manage.py migrate

or if the project was already there before CCTorrent
you may update the database:
python manage.py syncdb

Now populate the database with some default entries:
python manage.py loaddata cctorrent


That's it :) Enjoy CCTorrent :)