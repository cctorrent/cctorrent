import Image as PILImage
#from cctorrent.settings import settings, django_settings
from cctorrent.apps import CCTorrentConfig, django_settings
from subprocess import PIPE
from cctorrent.models import Album, Artist, Track, License, UploadedAudioFile
import hashlib
from cctorrent.ConverterThread import ConverterThread


def newUploadedTrack(trackData, track):
  #$requestFile is a file stored in request.FILES
  #track and album are a database model objects
  md5sum = hashlib.md5(trackData).hexdigest()
  
  uploadedFile = UploadedAudioFile()
  uploadedFile.track = track
  uploadedFile.album = track.album
  uploadedFile.fileName = md5sum+'.audio'
  uploadedFile.save()
  
  #now we read and store the file:
  f = open(CCTorrentConfig.FileDirectory+'convert/'+md5sum+'.audio', 'wb')
  f.write(trackData)
  f.close()
  
  #if everything worked fine until here we can set the file ready to be converted:
  uploadedFile.uploaded = True
  uploadedFile.save()
  c = ConverterThread(uploadedFile)
  c.start()
  

def convertUploadedImage(requestFile):
  #$requestFile is a file stored in request.FILES 
  #this function returns an python imaging library Image object
  uploadedImage = PILImage.open(requestFile)
  width, height = uploadedImage.size
  print "File uploaded with dimensions: "+str(width)+"x"+str(height) #DEBUG
  print "File type: "+str(uploadedImage.format)
  
  wCut = 0
  hCut = 0
  if(width > height):
    #widescreen (4:3, 16:9, ...)
    wCut = (width-height)/2 #we want to cut from each corner, so division by two is added
  elif(width < height):
    #3:4, 9:16, ...
    hCut = (height-width)/2
  
  newImage = None
  
  if (hCut == wCut == 0):
    #already square format
    newImage = uploadedImage.resize((CCTorrentConfig.SquareImageSize,CCTorrentConfig.SquareImageSize))
  else:
    newImage = uploadedImage.crop((wCut, hCut, width-wCut, height-hCut))
    newImage = newImage.resize((CCTorrentConfig.SquareImageSize,CCTorrentConfig.SquareImageSize))

  return newImage