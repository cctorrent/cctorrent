import time, hashlib, os

#
#   This file is part of cctorrent
#   Copyright (C) 2014-2015 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


#sources:
#  https://wiki.theory.org/BitTorrentSpecification
#  http://stackoverflow.com/questions/17056382/read-file-in-chunks-ram-usage-read-strings-from-binary-files


def bencode(item = None):
  output = ""
  
  #now let's have a look what data type we have got in item:
  if(isinstance(item, dict)):
    #a dictionary loop over all items and call bencode recursively:
    output = 'd'
    for k in item:
      try:
        output += bencode(k)+bencode(item[k])
      except Exception as e:
        print("Exception at key "+str(k)+" or value "+item[k])
    output += 'e'
    return output
  
  elif(isinstance(item, list)):
    #a list: loop over all items and call bencode recursively:
    output = 'l'
    for k in item:
      try:
        output += bencode(k)
      except Exception as e:
        print("Exception at key "+str(k))
    output += 'e'
    return output
  
  elif(isinstance(item, int)):
    #integer:
    return 'i'+str(item)+'e'
  
  elif(isinstance(item, str)):
    #string:
    return str(len(item))+":"+item
  
  else:
    raise Exception("Bennicode.bencode: Unsupported object type!")




class TorrentFile():
  def __init__(self, name = None, length = 0, md5sum = None, path = None, torrentPath=None):
    self.name = ""
    self.length = 0
    self.md5sum = ""
    self.path = "" #the path to the file in the file system (not in the torrent), including file name!
    self.pathList = [] #first list item = first directory in torrentPath
    
    if(isinstance(name, basestring)):
      self.name = str(name)
    if(isinstance(length, int)):
      self.length = length
    if(isinstance(md5sum, basestring)):
      self.md5sum = md5sum
    if(isinstance(path, basestring)):
      self.path = str(path)
    if(isinstance(torrentPath, basestring)):
      self.pathList = torrentPath.split('/') #Windows path names not supported by design!
    
  def toDict(self):
    #only if there are multiple files
    d = dict()
    d["length"] = self.length
    if(self.md5sum != ""):
      d["md5sum"] = self.md5sum
    
    #there are more than one file in the torrent. Each one of them has a path:
    d["path"] = self.pathList
    d["path"].append(self.name)
    
    return d
    

class Torrent():
  def __init__(self, name = None, baseDir = None, files = None, trackerURL = None, comment = None, creator = None, pieceLength = None, private = True):
    self.name = "" #only useful when putting multiple files in one torrent
    self.baseDir = None #the base dir of the torrent
    self.files = []
    self.trackerURLs = []
    self.comment = ""
    self.creator = ""
    self.pieceLength = 131072 #128 KiB by default
    self.private = True
    self.webSeeds = []
    if(isinstance(name, basestring)):
      self.name = str(name)
    if(isinstance(baseDir, basestring)):
      self.baseDir = str(baseDir)
    if(isinstance(files, list)):
      for f in files:
        if(not isinstance(f, TorrentFile)):
          raise Exception("Error: Torrent.files has to be a list of TorrentFile objects!")
      self.files = files
    if(isinstance(trackerURL, basestring)):
      self.trackerURLs.append(str(trackerURL))
    
    self.creationDate = int(time.time())
    
    if(isinstance(comment, basestring)):
      self.comment = str(comment)
    if(isinstance(creator, basestring)):
      self.creator = str(creator)
    if(isinstance(pieceLength, int)):
      if(pieceLength > 1023): #at least 1 KiB
        self.pieceLength = pieceLength
      else:
        print("WARNING: piece length was set to default since the given piece length was less than 1024 bytes!")
    if(isinstance(private, bool)):
      self.private = private
  
  
  def AddWebSeed(self, seedURL=None):
    if(seedURL != None):
      self.webSeeds.append(str(seedURL))
  
  def AddTracker(self, trackerURL=None):
    if(trackerURL != None):
      self.trackerURLs.append(str(trackerURL))
  
  def AddFile(self, path, torrentPath=None):
    #adds a file to the torrent:
    #print("Opening path " + path)
    try:
      f = open(path, 'rb')
      md5sum = hashlib.md5(f.read()).digest()
      f.close()
      #print("DEBUG: length of path \""+path+"\" = "+str(os.stat(path).st_size))
      t = TorrentFile(name=os.path.basename(path), length=int(os.stat(path).st_size), md5sum=md5sum, path=path, torrentPath=self.baseDir)
      self.files.append(t)
    except Exception as e:
      raise Exception("Error adding file from "+path+":"+str(e))
  
  
  def CalculateChecksums(self):
    #calculate the checksums for all pieces:
    
    flist = []
    for tf in self.files:
      #first we open all files:
      try:
        #print ("Opening "+tf.path+"...")
        flist.append(open(tf.path, 'rb'))
      except Exception as e:
        #print ("ERROR: cannot open file "+tf.path)
        raise Exception("Cannot open file "+tf.path)
    
    sha1sums = []
    block = '' #a binary data block of length $self.pieceLength (or shorter)
    for f in flist:
      #now we calculate the sha1sums of all blocks separately
      while True:
        if(len(block) != 0):
          #there were bytes left from the last iteration
          leftBytes = self.pieceLength - len(block)
          if(leftBytes > 0):
            block += f.read(leftBytes)
        else:
          #no bytes left from the last iteration
          block += f.read(self.pieceLength)
        
        if(len(block) == self.pieceLength):
          #the block is full: calculate the sha1sum:
          sha1sums.append(hashlib.sha1(block).digest())
          #...and flush the content of $block:
          block = ''
        else:
          #the block is not full: it seems this file cannot fill the block so we exit the endless loop
          break
      
      #if this is the last element in the file list we have to calculate the sha1sum for the remainder:
      if(f == flist[-1]):
        if(len(block) > 0):
          #only append the remainder if the block has bytes in it:
          sha1sums.append(hashlib.sha1(block).digest())
    
    #close all files:
    for f in flist:
      f.close()
    
    #concatenate all sha1sums into one big sum:
    bigsum = dict()
    #print("DEBUG: Torrent:CalculateChecksums: len(sha1sums) = "+str(len(sha1sums)))
    bigsum["l"] = len(sha1sums)
    bigsum["d"] = ''
    for s in sha1sums:
      bigsum["d"] += s
    return bigsum
  
  
  def Bencode(self):
    #creates a bencode representation of this torrent file
    d = dict()
    
    info = dict()
    info["piece length"] = self.pieceLength
    
    #TODO: return data (pieces[d]) directly when it is verified that the calculated checksums are correct
    pieces = self.CalculateChecksums() #get sha1 hashes from each file and store them here as one big sum
    
    info["pieces"] = pieces["d"]
    
    if(self.private == True):
      info["private"] = 1
    else:
      info["private"] = 0
    
    if(len(self.files) == 1):
      info["name"] = self.files[0].name
      info["length"] = self.files[0].length
      info["md5sum"] = self.files[0].md5sum
    elif(len(self.files) > 1):
      info["name"] = self.name 
      info["files"] = []
      for f in self.files:
        info["files"].append(f.toDict())
    d["info"] = info
    
    if(len(self.trackerURLs) > 0):
      d["announce"] = self.trackerURLs[0] #for backward compatibility: the first tracker is placed in here
    if(len(self.trackerURLs) > 1):
      d["announce-list"] = list()
      d["announce-list"].append(self.trackerURLs)
    
    d["creation date"] = self.creationDate
    d["comment"] = self.comment
    d["created by"] = self.creator
    
    if(len(self.webSeeds) == 1):
      #only one web seed: add it as a string
      d["url-list"] = self.webSeeds[0]
    elif(len(self.webSeeds) > 1):
      #there are more than one web seeds:
      d["url-list"] = self.webSeeds
    
    try:
      return bencode(d)
    except Exception as e:
      print("Error bencoding dictionary! d="+str(d))
  