#
#   This file is part of cctorrent
#   Copyright (C) 2014-2015 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from django.conf.urls import patterns, include, url


cctorrent_urls = patterns('',
    # Examples:
    # url(r'^$', 'cctorrent.views.home', name='home'),
    # url(r'^cctorrent/', include('cctorrent.foo.urls')),
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    
    # Uncomment the next line to enable the admin:
    
    url(r'^$', 'cctorrent.views.index', name='index'),
    url(r'^api', 'cctorrent.views.api', name='api'),
    url(r'^js/settings.js', 'cctorrent.views.settingsJS', name='settingsJS'),
    
    url(r'^help/TorrentClients', 'cctorrent.views.torrentClients', name='TorrentClients'),
    
    #url(r'^login', 'cctorrent.views.loginView', name='login'),
    url(r'^login', 'django.contrib.auth.views.login', {'template_name': 'cctorrent/login.html'}, name='login'),
    url(r'^logout', 'django.contrib.auth.views.logout', {'template_name': 'cctorrent/logout.html'}, name='logout'),
    
    url(r'^artist/(\d+)/$', 'cctorrent.views.artist', name='artist'),
    url(r'^artist/(\d+)/newalbum', 'cctorrent.views.newalbum', name='newalbum'),
    
    url(r'^album/(\d+)/$', 'cctorrent.views.album', name='album'),
    url(r'^album/(\d+)/finish', 'cctorrent.views.finishAlbum', name='finishAlbum'),
    
    url(r'^album/(\d+)/newtrack', 'cctorrent.views.newtrack', name='newtrack'),
    
    url(r'^tag/([a-z0-9]+)/$', 'cctorrent.views.tagsearch', name='tagsearch'),
    url(r'^search', 'cctorrent.views.search', name='search'),
    url(r'^search', 'cctorrent.views.search', name='searchresult'),
    #url(r'^searchresult', 'cctorrent.views.search', name='searchresult'),
    url(r'^track/(\d+)/', 'cctorrent.views.track', name='track'),
    
    url(r'^user/(\d+)/newartist', 'cctorrent.views.newartist', name='newartist'),
    url(r'^user/(\d+)/', 'cctorrent.views.user', name='user'),
    url(r'^dashboard', 'cctorrent.views.dashboard', name='dashboard'),
    url(r'^importJamendo', 'cctorrent.views.importJamendo', name='importJamendo'),
    url(r'^usersettings', 'cctorrent.views.usersettings', name='usersettings'),
    
    #TODO: serve the following urls as static files => DONE!
    
    #CSS URLs:
    #url(r'^styles/basic', 'cctorrent.views.basicStyle', name='BasicStyle'),
    #url(r'^styles/color', 'cctorrent.views.colorScheme', name='ColorScheme'),
    
    #Logo URL:
    #url(r'^logo.png', 'cctorrent.views.logo', name='Logo'),
)
