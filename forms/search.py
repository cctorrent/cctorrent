from django import forms

class GeneralSearchForm(forms.Form):
  term = forms.CharField(max_length=150)
  
